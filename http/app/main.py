#!/usr/bin/env python

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import time
import uuid
from flask import Flask,abort,request,render_template,make_response
app = Flask(__name__)

class Keks():
    def __init__(self,title,text):
        self.title = title
        self.text = text


@app.route("/")
def hello():
    return render_template('index.html')

@app.route("/post",methods=['POST'])
def postonly():
    return render_template('index.html')

@app.route("/images",methods=['GET'])
def images():
    return render_template('images.html')

@app.route("/coffee",methods=['GET'])
def coffee():
    abort(418)

@app.route("/feed",methods=['GET'])
def feed():
    time.sleep(1) # deplay 1 second
    response = make_response( render_template('feed.html') )
    response.cache_control.no_cache = True
    response.cache_control.no_store = True
    response.cache_control.max_age = 0
    response.set_cookie('XXX_Tracker',str(uuid.uuid1()))
    return response

@app.route("/kekse",methods=['GET','POST'])
def kekse():
    ret = ""
    entries = []
    for keks in request.cookies:
        entries.append( Keks(keks,request.cookies[keks]) )

    resp = make_response( render_template('kekse.html',entries=entries) )

    if request.method == 'POST':
        nk = Keks(request.form['title'],request.form['text'])
        resp.set_cookie(nk.title, nk.text,domain=request.host)

    return resp

if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=8080)
