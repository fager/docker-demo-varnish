#!/bin/sh

case $1 in
  start)
    /usr/sbin/varnishd -a 0.0.0.0:80 -f /etc/varnish/default.vcl -F
  ;;

  log)
    /usr/bin/varnishncsa
  ;;

  detaillog)
    /usr/bin/varnishlog
  ;;
  banner|version)
    /usr/bin/varnishadm banner
  ;;
  labs)
    case $2 in
      load)
        cd /labs/ || exit 1
        for lab in lab*.vcl
        do
          LABNAME="$(basename "${lab}" .vcl)"
          echo "Loading lab: ${LABNAME}"
          /usr/bin/varnishadm vcl.load "$LABNAME" "/labs/$lab"
        done
        cd ..
      ;;
      list)
        /usr/bin/varnishadm vcl.list
      ;;
      lab*)
        if test -f "/labs/${2}.vcl"
        then
          /usr/bin/varnishadm vcl.use "${2}"
        else
          echo "Lab not found"
        fi
      ;;
      show)
        /usr/bin/varnishadm vcl.show "$3"
      ;;
      *)
        cat <<EOH
  Usage:
    /varnish.sh labs ...
           load       load the predifined configurations
           list       list all configurations
           show CFG   show the configuration for CFG
           labX       activate configuration X
EOH
      ;;
    esac
  ;;

  *)
    cat <<EOH

Usage:
  /varnish.sh [log|detaillog|banner|labs]
EOH
  ;;
esac


