# backend switch based on hostname
vcl 4.0;

# Default backend definition. Set this to point to your content server.
backend default {
    .host = "nginx";
    .port = "8080";
}

backend http {
    .host = "http";
    .port = "8080";
}

sub vcl_recv {

    if (req.http.host ~ "^http") {
        set req.backend_hint = http;
    } else {
        set req.backend_hint = default;
    }
}

