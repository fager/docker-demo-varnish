# blanko example for one backend
vcl 4.0;

import std;

# Default backend definition. Set this to point to your content server.
backend default {
    .host = "http";
    .port = "8080";
}

sub vcl_recv {
    unset req.http.proxy;

    # Normalize the query arguments
    set req.url = std.querysort(req.url);

    # Remove Cookies from static files
    if (req.url ~ "^[^?]*\.(css|gif|gz|ico|jpeg|jpg|js|png|xml)(\?.*)?$") {
        unset req.http.Cookie;
    }

    # get everything not GET or HEAD direct from backend
    if (req.method != "GET" && req.method != "HEAD") {
        return (pass);
    }

    # Set informations about the original client ip
    if (req.restarts == 0) {
        if (req.http.x-forwarded-for) {
            set req.http.X-Forwarded-For =
            req.http.X-Forwarded-For + ", " + client.ip;
        } else {
            set req.http.X-Forwarded-For = client.ip;
        }
    }
}

sub vcl_backend_response {
    # cache the response for 1h if backend is not available
    set beresp.grace = 1h;

    if (bereq.url ~ "^/feed" || bereq.url ~ "/static/.*(png|jpg|jpeg)$") {
        # remove response cookies
        unset beresp.http.Set-Cookie;

        # ignore the Cache-Control settings from the backend
        unset beresp.http.Cache-Control;

        # cache response for 9s
        set beresp.ttl = 9s;
    }
}

