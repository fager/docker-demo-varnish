# README

This is a simple Varnish setup build only for demonstration.

DO NOT USE THIS FOR PRODUCTION !!!


## Requirements

- docker
- docker-compose

or

- podman
- podman-compose

## CentOS 8

Prepare your CentOS 8 System for this demo (every other system with docker should be fine too...)


```
dnf module enable container-tools:3.0
dnf module install container-tools:3.0
dnf install podman-docker git
```

[Install Docker Compose](https://docs.docker.com/compose/install/)


